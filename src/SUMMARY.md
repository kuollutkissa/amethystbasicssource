# Summary
1. [Introduction](introduction.md)
1. [Objects](objects.md)
1. [Entities](entities.md)
    1. [Basic stats](./entities/basic-stats.md)
    1. [Player characters](./entities/characters.md)
1. [Activities and events](activities-n-events.md)
    1. [Combat](./activities/combat.md)
    1. [Sneaking & stealth](./activities/sneaking.md)