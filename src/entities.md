# Entities
Entities are any creatures, animals, people and automatons inhabiting the world. Each entity must have:
1. Stats & skills, outlined in the next subchapter
1. A body part breakdown, if an entity has only one part then obviously that part is to be described

They might also posess **additional properties**, i.e. inventories, inner dimensions; the sky is the limit.

## Body part breakdown
Each body part should have at minimum a **minimum hit score** and a **damage multiplier** (both integers in [0, 65535]).
The minimum hit score is the value which the aim score must attain to hit that body part.
The damage multiplier is an integer used to calculate how much damage attacking the body part deals to the entity.
Each body part can also have **special events** which trigger when the part is damaged or lost.
