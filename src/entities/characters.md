# Characters
Player characters are a special type of entity controlled by the player.

## Inventory
Each character has an inventory in where they can store equipment, supplies, loot etc. The consideration whether one should be able to carry all the things excluding the **weight limit**  shall be left to individual interpretation with common sense in mind, as in one cannot carry 7000 pens without a backpack.
## Special abilities
Characters might have some **special abilities** which are left to the users' creativity.
## Status effects
Characters may carry status effects which could include illnesses, potion effects etc. These either apply a modifier or are taken into consideration in specific circumstances.