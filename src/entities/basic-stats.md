# Basic stats

## Stats
Stats are any properties that depend heavily on the multitudes of groups (species, races, classes, professions) an entity can be assigned to.
### Health
Pretty self-explanatory I believe, if a character's HP achieves 0 they die. \
It is represented with an integer with the suggested value for an **average human man being 100**.

### Intelligence
Ability to think critically, solve problems, logic etc. \
It is represented with an integer in range [0, 255] with the **average person being around 130**
### Durability
How resilient to damage the character is *out of the box*, without any armors or clothes taken into consideration. \
Stored as an integer in range [0, 255] with the average adult human being at circa 80.

More can be added depending on the needs of the setting/campaign.
## Skills
Skills are "learnable" properties, they are more variable between individuals of the same groups.

### Physical strength
Matters in physical combat and in any physically demanding activities. The value of strength directly describes how many **kilograms** of objects the character can carry around. \
It is represented with a real number.
### Dexterity
Grace in physical movement, especially hands. Matters in any manual tasks like tinkering with machinery. \
It is represented with an integer in range [0, 255] with the **average person being around 100**
### Agility
Grace and precision in physical movement of the whole body. Matters in acrobatics and evading attacks. \
It is represented with an integer in range [0, 255] with the **average person being around 100**
### Speed
Self-explanatory I believe. \
It is represented with an integer in range [0, 255] with the **average person being around 100**
### Visual Perceptivity
Ability to notice things visually. Matters when looking for something etc.
It is represented with an integer in range [0, 255] with the **average person being around 130**
### Auditory Perceptivity
As above but related to hearing. \
It is represented with an integer in range [0, 255] with the **average person being around 130**
### Physical sensitivity
How sensitive your character is to physical touch. **This is inversely proportional to pain tolerance.** <br>
It is represented with an integer in range [0, 255] with the **average person being around 130**
### Stamina 
Ability to continue physical exercise. \
It is represented with an integer in range [0, 255] with the **average person being around 100**

## Modifiers
Modifiers can be applied to any stat or skill by environments, objects, status effects etc. They temporarily modify the value of the modified object.
