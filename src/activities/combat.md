# Combat
Each time a party enters combat the GM should provide a description of the battlefield either in text, picture or 3D model with a clear way to discern the distances between combatants.

The players should roll a **D10** to determine the order of action with higher scores meaning a higher spot in the queue. The players then take turns describing their actions and the oponents do the same. The length of a turn is **loosely around a second or less**.
## Meelee combat
In meelee combat, the player should choose what they are aiming for. After choosing their target the player should roll a **D20**. The aim score is calculated by computing the arithmetic average of *dexterity* and *visual perceptivity* with integer division[^1] and adding the roll.The opponent then rolls a **D20**. Then the dodge score is calculated `agility + roll`.

If the aim score exceeds the opponent's dodge score and the part's minimum hit score the attack succeeded and the player should now roll a **D10** for damage. The character attack score is then computed from the character's strength: `strength div 10`. If the player uses a weapon its score is also computed according to its description. The two scores are then added to the roll and this is the **attack score**. The defendant should then roll a **D10** for defence. They then add to that their armor score and if an armor is present, armor score.

The defence score should then be subtracted from the attack score. If the resultant value is greater than 0, it is then multiplied by the body part's **damage multiplier** and `div`ed by 255 and the resultant value is the damage dealt.
## Ranged combat
In ranged combat, the player should choose what they are aiming for.  After choosing their target the attacker should roll a **D20**. The aim score is calculated: `(dexterity + visual perceptivity) div 2`. It is then `div`ed by the distance in meters rounded up. 
If the aim score exceeds the body part's minimum hit score, the attacker rolls a **D10** for damage. It is then added to the calculated ranged weapon score.
The opponent rolls then for defence and adds their durability and armor score. Damage is calculated as in meelee combat.

## Other types of combat
Specific settings and campaigns might implement special combat types including special stats and special outcomes. An example of a system not covered here is psychic combat.


[^1] in integer division any non-integer part is discarded so `5 div 2` equals `2` and `2388 div 100` equals `23`. For the rest of this book we will use `div` to denote this operation.