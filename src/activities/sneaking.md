# Sneaking
Sneaking involves doing anything while avoiding being spotted by an opponent. During such actions any time a character decides to do anything they and *the watcher* both roll a **D10**. The sneaker should add to that score their agility and any modifiers from abilities, items etc. for both visual and auditory clues and the watcher should add their visual and auditory perceptivity. Whoever gets a bigger score succeeds.

A campaign may add another stealth *plain*, perhaps somethign like physic powers or electronics/radio.