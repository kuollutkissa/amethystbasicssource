# Custom activities
GMs and setting designers may introduce custom activities fitting for their campaign. An event can either be RNG-based, employing a stat or an ability and a die throw or may be a special minigame. In the former case, a minimum *success score* is needed to decide whether the activity was successful, in the latter the GM is to decide. 

A die throw activity has to have the following:
    - a name
    - a success score
    - a list of dice required
    - the stats to consider
    - the formula to calculate the score

