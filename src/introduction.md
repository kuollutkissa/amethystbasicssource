# Introduction
Amethyst is a general-purpose TTRPG system created mostly just because I felt like it and it'll probably remain in personal use. My experience with TTRPGs is limited to being the player and I'm not very familiar with any system so this is halfway to being written from scratch. \
Reasons:
- Have fun
- Create a completely open TTRPG system ~~rewrite d20 in rust~~
- Create a system for my personal projects

The system uses:
- Dice (D4, D6, D8, D10, D12, D20)
- Pen & paper
- A calculator
---
If you feel like checking it out I'm positively suprised and I wish happy reading.

 The Amethyst TTRPG System Template © 2025 by Kuollutkissa is licensed under [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1) 
