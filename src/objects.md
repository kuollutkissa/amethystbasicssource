# Objects
Objects are a very loose category that encompasses anything that characters, NPCs and creatures may hold or what might be placed somewhere in the world. They may be divided into several types:
## Equipment
This type includes any weapon, tool or apparel. Usually equipment will provide some **extra points** in a certain activity or situation (i.e. meelee weapons improve the attack score in meelee combat or armor increases the defence score). Equipment should usally have a **durability** represented with an integer in [0, 255]
## Consumables
The name says it all. This type encompasses any food, potions, drugs and whatever your conjure up. Consumables can either have a **discrete quantity** represented with an integer or a **mass quantity** represented with real number.
## Items
Items are various objects that don't fit in either of the previous categories. This type could include stuff like ingredients, random sticks and rocks found on the way, artifacts, MacGuffins etc.
## Trinkets
Trinkets are any items whose mass is **irrelevant to the calculation of how much a character can carry** of your character, stuff like jewelery, amulets, notes etc.